;;;; SEL interface to LIBFUZZER
;;;
;;; Stages:
;;; 1. Write a harness for the target function
;;; 2. Compile the target function into a fuzzer executable
;;; 3. (optional) Convert tests for the target function into a corpus of binary inputs
;;; 4. Execute the linked fuzzer executable
;;;    (At this point could also run AFL++ or Radamsa against the fuzz target)
;;; 5. Return updated test inputs from the corpus
;;;
(uiop/package:define-package :libfuzzer/libfuzzer
    (:nicknames :libfuzzer)
  (:use :gt/full
        :software-evolution-library
        :software-evolution-library/software/compilable
        :software-evolution-library/software/parseable
        :software-evolution-library/software/tree-sitter
        :software-evolution-library/command-line
        :stefil+)
  (:export :harness :build :fuzz :*crashing-inputs* :crashing-fuzz-inputs
           ;; Test suite
           :test))
(in-package :libfuzzer)
(in-readtable :curry-compose-reader-macros)

;;; Add -fsanitize=fuzzer during compilation with clang:
;;; clang -g -O1 -fsanitize=fuzzer                         mytarget.c # w/o sanitizers
;;; clang -g -O1 -fsanitize=fuzzer,address                 mytarget.c # with ASAN
;;; clang -g -O1 -fsanitize=fuzzer,signed-integer-overflow mytarget.c # with a part of UBSAN
;;; clang -g -O1 -fsanitize=fuzzer,memory                  mytarget.c # with MSAN

;;; TODO: this is relying on a simple build and likely won't work for projects?
;;; TODO: the file libfuzzer generates also needs trashed and/or directed to
;;;       /dev/tmp
;;; TODO: look into feeding output from DIG in as a corpus.
;;;       There's also a possibility of using tracing on a test suite to
;;;       generate a corpus.
;;; TODO: should probably limit the number of runs this can take; otherwise it
;;;       may end up looping indefinitely.

(defgeneric convert-to-fuzz-arguments (function)
  (:documentation "Return a string to pass standard fuzz arguments to FUNCTION.")
  (:method ((function-ast function-ast) &aux (index 0))
    ;; Satisfy each parameter of function-ast with something from Data and size.
    ;;
    ;; NOTE: This should be significantly improved down the line and
    ;;       could end up being the bulk of this repository.
    ;; TODO: reimplement array support.
    ;;       Figure out how to get the size of short, int, long, size_t, etc.
    ;;       There's likely something in the cffi to do this.
    (labels ((get-source-string (type depth size)
               "Get the source string for TYPE of DEPTH and SIZE."
               (if (zerop depth)
                   (prog1 (format nil "Data[~d]" index) (incf index size))
                   (error "Arrays unsupported for the given type: ~a[~a]" type depth))))
      (mapcar [(op (ematch _1
                     ((list* (and type (or "char" "uint8_t" "int8_t")) depth _)
                      (get-source-string type depth 1))
                     ((list* (and type (or "uint16_t" "int16_t")) depth _)
                      (get-source-string type depth 2))
                     ((list* (and type (or "uint32_t" "int32_t")) depth _)
                      (get-source-string type depth 4))
                     ((list* (and type (or "uint64_t" "int64_t")) depth _)
                      (get-source-string type depth 8))))
               #'parameter-type*]
              (function-parameters function-ast)))))

(defgeneric pointers (c-declarator)
  (:documentation "Return the number of pointers around C-DECLARATOR.")
  (:method ((ast c-parameter-declaration)) (pointers (c-declarator ast)))
  (:method ((ast c-pointer-declarator)) (1+ (pointers (c-declarator ast))))
  (:method ((ast c-identifier)) 0))

(defmethod parameter-type* ((ast c-parameter-declaration))
  "Return format is (BASE-TYPE POINTER-DEPTH . QUALIFIERS)."
  (list* (source-text (c-type ast))
         (pointers ast)
         ;; This assumes that ordering doesn't matter for
         ;; _declaration_specifiers.
         (mapcar
          #'source-text
          (append (c-pre-specifiers ast) (c-post-specifiers ast)))))

(defgeneric convert-from-fuzz-arguments (function crashing-input)
  (:documentation "Return a mapping of a parameter of FUNCTION and its
associated value in CRASHING-INPUT.")
  (:method ((function-ast function-ast) (crashing-input array))
    (let ((character-stack (coerce crashing-input 'list))
          (parameters (function-parameters function-ast)))
      (labels ((get-characters (type-string depth size)
                 "Get the source string for TYPE of DEPTH and SIZE."
                 (if (zerop depth)
                     ;; TODO: is there a popn somewhere?
                     (prog1 (list* (car type-string) (take size character-stack))
                       (setf character-stack (drop size character-stack)))
                     (error "Arrays unsupported for the given type: ~a" type-string))))
        (mapcar
         (lambda (type-string)
           (ematch type-string
             ((list* (or "char" "uint8_t" "int8_t") depth _)
              (get-characters type-string depth 1))
             ((list* (or "uint16_t" "int16_t") depth _)
              (get-characters type-string depth 2))
             ((list* (or "uint32_t" "int32_t") depth _)
              (get-characters type-string depth 4))
             ((list* (or "uint64_t" "int64_t") depth _)
              (get-characters type-string depth 8))))
         (mapcar #'parameter-type* parameters))))))

(defun convert-to-source-representation (output-cons)
  "Convert OUTPUT-CONS into a string which can be used as source."
  (labels ((convert-to-integer (bytes &optional (i 0) (result 0))
             "Convert BYTES to an integer. This assumes little endianess."
             (if bytes
                 (convert-to-integer
                  (cdr bytes)
                  (1+ i)
                  (dpb (car bytes) (byte 8 (* i 8)) result))
                 result)))
    (let ((type (car output-cons))
          (bytes (cdr output-cons)))
      (format nil "(~a) ~a" type (convert-to-integer bytes)))))

(define-constant +llvm-fuzz-target-name+ "LLVMFuzzerTestOneInput"
  :test #'string=)

(defgeneric harness-function (ast fuzz-arguments)
  (:documentation "Create a harness function for AST with FUZZ-ARGUMENTS.")
  (:method ((ast c-ast) fuzz-arguments)
    (convert 'c-ast
             (format nil "~%int ~A(const uint8_t *Data, size_t Size){
  ~A(~A);
  return 0;
}~%"
                     +llvm-fuzz-target-name+
                     (function-name ast)
                     fuzz-arguments)
             :deepest t))
  (:method ((ast cpp-ast) fuzz-arguments)
    (convert 'cpp-ast
             (format nil "~%extern \"C\" int ~A(const uint8_t *Data, size_t Size){
  ~A(~A);
  return 0;
}~%"
                     +llvm-fuzz-target-name+
                     (function-name ast)
                     fuzz-arguments)
             :deepest t)))

(defgeneric harness (software ast)
  ;; NOTE: there's a possibility that some function ASTs are lambdas
  ;;       it may be possible to pull these out into a handler function
  ;;       and test the lambda there directly.
  (:documentation "Write a fuzzing harness for AST in SOFTWARE.")
  (:method ((c c) (function-ast function-ast))
    (labels ((add-harness-dependencies (c)
               (let ((stdio-ast
                       (convert 'c-ast (format nil "#include <stdio.h>~%")
                                :deepest t))
                     (stdint-ast
                       (convert 'c-ast (format nil "#include <stdint.h>~%")
                                :deepest t)))
                 (splice c function-ast (list stdio-ast stdint-ast))))
             (add-harness-function (c function-ast)
               (let* ((parent (get-parent-ast c function-ast))
                      (child-number (1+ (cdr (lastcar (position function-ast parent)))))
                      (fuzz-arguments (string-join (convert-to-fuzz-arguments function-ast) ", ")))
                 (with c parent
                       (copy parent :children
                             (append (take child-number (direct-children parent))
                                     (list (harness-function function-ast fuzz-arguments))
                                     (drop child-number (direct-children parent))))))))
      (add-harness-dependencies
       (add-harness-function c function-ast)))))

(defgeneric build (software &key bin asan msan ubsan)
  (:documentation "Compile a fuzzing harness executable for SOFTWARE.
Keyword arguments:
- BIN specifies the path to which the resulting binary executable is compiled
- ASAN enables AddressSanitizer
  https://clang.llvm.org/docs/AddressSanitizer.html
- UBSAN enables UndefinedBehaviorSanitizer
  https://clang.llvm.org/docs/UndefinedBehaviorSanitizer.html
- MSAN enables MemorySanitizer
  https://clang.llvm.org/docs/MemorySanitizer.html")
  (:method ((c c) &key (bin nil binp) asan msan ubsan)
    (pushnew (concatenate 'string
                          "-fsanitize="
                          (string-join (append '("fuzzer")
                                               (when asan (list "address"))
                                               (when msan (list "memory"))
                                               (when ubsan (list "signed-integer-overflow")))
                                       ","))
             (flags c) :test #'string=)
    (setf (compiler c) "clang")
    (apply #'phenome c (when binp (list :bin bin)))))

(defgeneric tests-to-corpus ())

(defgeneric corpus-to-tests ())

(defvar *crashing-inputs* nil
  "Variable to dynamically collect crashing inputs found while fuzzing.")

(defun handle-crashing-input (path)
  "Default handler as crashing inputs are found while fuzzing.
Pushes inputs onto `*crashing-inputs*'."
  (pushnew path *crashing-inputs* :test #'string=))

(defgeneric fuzz (software &key &allow-other-keys)
  (:documentation "Fuzz SOFTWARE to find crashing inputs.")
  (:method ((c c) &rest args) (apply #'fuzz (build c) args))
  (:method ((string string) &rest args)
    (assert (probe-file string) (string) "string argument to `fuzz' is not a pathname: ~S" string)
    (apply #'fuzz (pathname string) args))
  (:method ((path pathname)
            &key output result-file inputs (crash-handler #'handle-crashing-input)
            &allow-other-keys)
    (assert (probe-file path) (path) "pathname argument to `fuzz' does not exist: ~S"
      path)
    (when inputs (warn "Ignoring inputs for now."))
    (io-shell (:error-output fuzzer-output
                             (fmt "~a ~@[-exact_artifact_path=~a~]"
                                  (namestring path)
                                  (namestring result-file)))
      ;; TODO: this iter likely isn't needed and io-shell
      ;;       should be switched to a direct launch-program call
      ;;       since it creates a file for output.
      (iter
        (for line := (read-line fuzzer-output nil :eof))
        (until (eql line :eof))
        (when output (write-line line output))
        (match line
          ((ppcre "Test unit written to (.+)" var)
           (funcall crash-handler var)))))
    (funcall crash-handler result-file)
    *crashing-inputs*))

(defgeneric crashing-fuzz-inputs (software ast &key inputs &allow-other-keys)
  (:documentation "Fuzz AST in SOFTWARE with optional INPUTS seeding the search
and return the crashing output if it exists.")
  (:method (software function-ast &key))
  (:method (software (function-ast function-ast) &key inputs &aux *crashing-inputs*)
    (with-temporary-file (:pathname result-file)
      (mapcar
       (lambda (bytes)
         (mapcar #'convert-to-source-representation
          (convert-from-fuzz-arguments function-ast bytes)))
       (mapcar #'file-to-bytes (fuzz (harness (copy software) function-ast)
                                     :inputs inputs
                                     :result-file result-file))))))


;;;; Test
(defvar *soft* nil "Holds software object for tests.")
(defvar *ast* nil "Holds an AST object for tests.")
(defvar *invariants* nil "Holds invairants for tests.")

(defroot test)

(defsuite test-libfuzzer "Test suite for libfuzzer")

(defixture simple-example
  (:setup (setf *soft*
                (from-file (make-instance 'c :compiler "clang")
                           (asdf:system-relative-pathname :libfuzzer "test/etc/test_fuzzer.cc"))
                *ast* (find-if {typep _ 'function-ast} *soft*)))
  (:teardown (setf *soft* nil *ast* nil)))

(deftest harness-simple-example ()
  (with-fixture simple-example
    (let ((harnessed-source (genome-string (harness *soft* *ast*)))
          (harnessed-function "look_for_hi((char*)Data, (unsigned)Size)"))
      (is (search harnessed-function harnessed-source)
          "Finds function invocation in harnessed source.~%Looking for ~S in:~%~A"
          harnessed-function harnessed-source)
      (is (search (format nil "extern \"C\" int ~A(const uint8_t *Data, size_t Size)"
                          +llvm-fuzz-target-name+)
                  harnessed-source)
          "Finds extern invocation in harnessed source."))))

(deftest build-harnessed-simple-example ()
  (with-fixture simple-example
    (harness *soft* *ast*)
    (with-temporary-file (:pathname bin)
      (build *soft* :bin bin)
      (is (probe-file bin)))))

(deftest fuzz-simple-example ()
  (with-fixture simple-example
    (is (some {scan "./crash-[a-z0-9]+"} (fuzz (harness *soft* *ast*))))))

(deftest fuzz-and-inject-seeds-yields-comments ()
  (with-fixture simple-example
    (is (search "// HI" (genome-string (fuzz-and-inject-seeds-as-comments *soft* *ast*))))))
