(defsystem "libfuzzer"
  :name "libfuzzer"
  :author "GrammaTech"
  :licence "MIT"
  :description "SEL interface to libFuzzer"
  :depends-on (:libfuzzer/libfuzzer)
  :class :package-inferred-system
  :perform (test-op (o c) (symbol-call :libfuzzer '#:test)))
